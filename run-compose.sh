#!/bin/bash

copy_apps_files() {
    cp -rf src/ docker/app/src/ &&\
	cp -f index.js package.json yarn.lock docker/app/ &&\
	cp -rf configs/ docker/app/configs/
}


start_services() {
    cd docker && docker-compose up --build -d
}

stop_services() {
    cd docker && docker-compose down
}


case $1 in
    "start")
	copy_apps_files && start_services
	;;
    "stop")
	stop_services
	;;
    *)
	echo 'please specify command like "start" or "stop"'
	;;
esac
