const mysql = require("mysql");
const {promiseable, execMethod, isNum, partial, isStr, count, map, reduce, selectKeys, get_, isObj, dropKeys} = require("fn-js");
const assert = require('assert').strict;

const connection = () => mysql.createConnection({
    host: process.env.DB_HOST || "localhost",
    user: process.env.DB_USER || "root",
    password: process.env.DB_ROOT_PASS || "mylocaldev",
    database: process.env.DB_NAME || "express_mysql"
});

const idempotentConnect = conn => {
    if (!conn._connectCalled) conn.connect();
    return conn;
};

const idempotentClose = conn => {
    if (conn._connectCalled) conn.end();
    return conn;
};

const exec = async (sql, values, fn) => {
    const conn = connection();
    try {
	idempotentConnect(conn);
 	const promisableConn = promiseable(execMethod(conn, "query"));
 	const result = await promisableConn(sql, values);
	conn.commit();
	idempotentClose(conn);
 	return fn ? fn(result) : result;
    } catch (error) {
	idempotentClose(conn);
 	throw error;
    }
};


const reUser = /^[a-zA-Z0-9_]{6,}$/;
const rePass = /^[a-zA-Z0-9_@#%!,=\-\?\$\^\&\*\.\+]{8,}$/;
const reName = /^[a-zA-Z\s]{5,}/;
const reGender = /^Male$|^Female$/;

const vUser = arg => reUser.test(arg);
const vPass = arg => rePass.test(arg);
const vName = arg => reName.test(arg);
const vGender = arg => reGender.test(arg);
const vId = arg => isNum(arg);


const everyPred = (pred, arg) => {
    if (!pred) return false;
    if (pred.length < 1) return false;
    let result;
    for (x = 0; x < pred.length; x++) {
	const p = pred[x];
	result = p(arg);
	if (!result) {
	    return result;
	}
    }
    return result;
};


const userSpec = {
    required: ["username", "password"],
    fields: {
	username: [isStr, vUser],
	password: [isStr, vPass],
	first_name: [isStr, vName],
	last_name: [isStr, vName],
	gender: [isStr, vGender]
    }
};

const userUpdateSpec = {
    required: ["id"],
    fields: {
	id: [vId],
	username: [isStr, vUser],
	password: [isStr, vPass],
	first_name: [isStr, vName],
	last_name: [isStr, vName],
	gender: [isStr, vGender],
    }
};


const assertSpec = ({required, fields}, data) => {
    const opt = dropKeys(data, required);
    if (required) {
 	map(field => {
	    const validators = get_(fields, field);
	    const value = get_(data, field);
 	    assert(everyPred(validators, value), `invalid required field ${field}: ${value}`);
 	}, required);
    }
    map(([k, v]) => {
	const validators = get_(fields, k);
	assert(everyPred(validators, v), `invalid field ${k}: ${v}`);
    }, opt);
    return true;
};


const isValid = (spec, data) => {
    try {
	return assertSpec(spec, data);
    } catch (error) {
	console.log(selectKeys(error, ["code", "message", "actual", "expected"]));
	return false;
    }
};

const dropUnused = ({required, fields}, data) => {
    return selectKeys(data, Object.keys(fields));
};

const createInsertStatement = (table, data) => {
    if (count(data) > 0) {
	const keys = Object.keys(data).toString();
	const vals = Object.values(data);
	const valsPlaceholder = vals.toString().replace(/[^,]+/g, "?");
	return [`insert into ${table}(${keys}) values(${valsPlaceholder})`, vals];
    } else {
	return null;
    }
};


const createUpdateStatement = (table, data) => {
    if (count(data) > 0) {
	const len = count(data);
	const withoutId = dropKeys(data, ["id"]);
	const body = map(([k, v]) => `${k}=?`, withoutId);
	const vals = Object.values(withoutId);
	return [`update ${table} set ${body} where id = ?`, [...vals, data.id]];
    } else {
	return null;
    }
};


const updateUser = async (data) => {
    assertSpec(userUpdateSpec, data);
    const toInsert = dropUnused(userUpdateSpec, data);
    const [statement, vals] = createUpdateStatement("USER_DATA", toInsert);
    return statement && vals ? await exec(statement, vals) : null;
};

const insertUser = async data => {
    assertSpec(userSpec, data);
    const toInsert = dropUnused(userSpec, data);
    const [statement, vals] = createInsertStatement("USER_DATA", toInsert);
    return statement && vals ? await exec(statement, vals) : null;
};

module.exports.exec = exec;
module.exports.insertUser = insertUser;
module.exports.updateUser = updateUser;
