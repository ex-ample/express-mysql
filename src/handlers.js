const fn = require('fn-js');

module.exports.getUserById = async ({req: {params}, deps: {mysql: {exec}}}) => {
    const [rows, fields] = await exec('select id, username, email, gender from USER_DATA where id = ?', [params.id]);
    return {
	res: {
	    body: rows[0]
	}
    };
};

module.exports.getUserTxbyUserId = async ({req: {params}, deps: {mysql: {exec}}}) => {
    const sql =
	`select USER_DATA.id as uid,  
            TX_DATA.id as id, 
            TX_DATA.product_id as product_id,
            TX_DATA.total as total
         from USER_DATA join TX_DATA 
         on USER_DATA.id = TX_DATA.user_id 
         where USER_DATA.id = ?
	`;
    const [rows, fields] = await exec(sql, [params.id]);
    return {
	res: {
	    body: rows
	}
    };
};

module.exports.getTransactionById = async ({req: {params}, deps: {mysql: {exec}}}) => {
    const sql =
	`select TX_DATA.id as id,
	   TX_DATA.total as total,
           PRODUCT_DATA.product_name as product_name,
           PRODUCT_DATA.product_price as product_price
         from TX_DATA 
         join PRODUCT_DATA 
         on TX_DATA.product_id = PRODUCT_DATA.id
         where TX_DATA.id = ?`;
    const [rows, fields] = await exec(sql, [params.id]);
    return {
	res: {
	    body: {...rows[0], total_price: (rows[0].product_price * rows[0].total)}
	}
    };
};


module.exports.getProductById = async ({req: {params}, deps: {mysql: {exec}}}) => {
    const sql = `select * from PRODUCT_DATA where id = ?`;
    const [rows, fields] = await exec(sql, [params.id]);
    return {
	res: {
	    body: rows[0]
	}
    };
};


module.exports.insertNewUser = async ({req: {body}, deps: {mysql: {insertUser}}}) => {
    return {
	res: {
	    body: (await insertUser(body))[0]
	}
    };
};


module.exports.updateUser = async ({req: {body, params}, deps: {mysql: {updateUser}}}) => {
    return {
	res: {
	    body: (await updateUser({...body, id: parseInt(params.id)}))[0]
	}
    };
};


module.exports.deleteUser = async ({req: {params}, deps: {mysql: {exec}}}) => {
    const sql = `delete from USER_DATA where id = ?`;
    const query  = `select id from USER_DATA where id = ?`;
    const [rows, fields] = await exec(query, [params.id]);

    return rows[0] ?
	   (rows[0].id ?
	    {res: {body: (await exec(sql, [params.id]))}} :
	    null) :
	   null;
};
