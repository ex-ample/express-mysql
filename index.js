const {exec, insertUser, updateUser} = require('./src/storage.js');
const fn = require('fn-js');
const {interceptor: intc, utils, dev} = require('rest2an');
const express = require("express");
const {routesConfigParser, interceptor} = intc;
const {readConfigX} = utils;
const {testRoute, autoTest, inspectLoadedRoute, assertResponse} = dev;

const runnableServer = express();

const routes = readConfigX('./configs/routes.json');

const deps = {
    mysql: {
	exec: exec,
	insertUser: insertUser,
	updateUser: updateUser
    }
};

const routeConfigurator = routesConfigParser(routes, deps);
runnableServer.use(express.json());
runnableServer.use(express.urlencoded({extended: false}));
runnableServer.use(express.text());
routeConfigurator(runnableServer);


runnableServer.use(async(err, req, res, next) => {
    if (err) {
	console.log(fn.selectKeys(err, ["name", "message", "code"]));
	res.status(500).send({message: "Internal Server Error"});
    } else {
	next();
    }
});


runnableServer.use(async(req, res, next) => {
    res.status(404).send({message: "Not Found"});
});

const nextCheck = (lastUsage, iterN) => {
    const currentUsage = process.memoryUsage().heapUsed;
    const threshold = 2 * 1024 * 1024;
    const isIncreased = (currentUsage - lastUsage) > threshold;
    if (iterN > 9) {
	if (isIncreased){
	    process.kill(process.pid, "SIGUSR2");
	    setTimeout(() => nextCheck(currentUsage, 0), 10000);
	} else {
	    setTimeout(() => nextCheck(currentUsage, 0), 10000);
	}
    } else {
	setTimeout(() => nextCheck(lastUsage, iterN + 1), 10000);
    }
};


if (process.env.NODE_ENV === 'production') {
    const server = runnableServer.listen(3000, () => console.log('server started on port 3000'));
    process.on('SIGTERM', () => {
	console.log('SIGTERM receive. Terminating server');
	server.close(() => {
	    console.log('Server terminated');
	    process.exit(0);
	});
    });

    process.on('SIGINT', () => {
	console.log('SIGINT receive. Terminating server');
	server.close(() => {
	    console.log('Server terminated');
	    process.exit(0);
	});
    });

    console.log(process.pid);
    nextCheck(process.memoryUsage().heapUsed, 0);
    
} else if (process.env.NODE_ENV === 'dev') {
    const server = runnableServer.listen(3000, () => {
	autoTest(routes, runnableServer);
    });
} else if (process.env.NODE_ENV === 'test'){
    autoTest(routes, runnableServer, err => {
	if (err){
	    throw err;
	} else {
	    process.exit(0);
	}
    });
} else {
    console.log('nothing to do');
}
